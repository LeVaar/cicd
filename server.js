var express = require('express');
             var bodyParser = require('body-parser');
              var calc = require('./routes/calculate');
              var app = express();
               app.use(bodyParser.json());
               app.use(bodyParser.urlencoded({ extended: false }));
               app.use(express.static("public"));
               app.use('/calculate', calc);
               module.exports = app;
app.listen(3000, function (){
	console.log('Example app on port 3000')
})
