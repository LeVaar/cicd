var request = require('supertest');
describe('GET /', function() {
	it('displays "Hello World!"', function(done) {
		request(app).get('/').expect('Hello World!');
		done();
	});
});